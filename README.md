<!-- Change the title of the class -->

# Class Title

---

### Class Goal

<!-- 4-6 sentences -->
Students should build a project using the methods and tools you teach in your class.  What is this project?  Why do we do it this way?

---

### Success Metric

<!-- 1-2 sentences -->
What is the most important way for students to measure the value of this class?

---

### Prerequisites:

<!-- Update this list to specify all the requirements for your class. -->
<!-- Define your expectations for the baseline student.  -->
<!-- Note: Using https://repl.it can significantly simplify requirements for programming-related classes. -->

- Familiarity with shell commands (e.g. `cd`, `ls`, etc.)
- Know how to eat a taco
- Laptop
  - Something with a Bash-like shell is preferred (Mac or Linux)
  - Install these tools before you arrive:
    - [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
    - [Atom](https://atom.io/) (or a similar text editor)
    - [Docker](https://hub.docker.com/search/?type=edition&offering=community)
- [Create a GitLab account](https://gitlab.com/users/sign_in)
  - If you already have one, you can disregard this
  - If you create a new one, you might consider making it something that you could take from company to company


---

<!-- Feel free to include/drop/modify this section at your discretion -->

### Following Instructions

Please follow the documented and stated instructions as closely as possible.  This will help mitigate issues that arise due to funky configurations.  The labs for this class are very inter-dependent, and an innocent deviation in an early lab could complicate a later lab.

I encourage all students to experiment and explore the material.  Making it your own and having fun with it increases the functional utility of this class immensely.  I'm happy to help with any extracurricular questions and/or interests related to the material, but please try the extra stuff *after* completing the suggested stuff.  And maybe in a different subdirectory.  :)

---

<!-- OPTIONAL Section -->
### Environment Setup

Sometimes it's helpful to have some basic setup instructions that will help manage students' environments throughout the labs.  If your class uses a site like [repl.it](https://repl.it), this could be unnecessary.

```bash
mkdir ~/sfs
cd ~/sfs
echo REPO_NAME="my-class" > .env
echo ANOTHER_VAR="something else" >> .env
source ~/sfs/.env
```

---

# Labs

<!-- OPTIONAL: Create a Table of Contents. -->
<!-- Make sure to keep this up to date as you make edits. -->
<!-- If you opt not to do this, a link to the first lab is helpful. -->

### 0. [Verify Prerequisites](labs/00_verify_prereq/Readme.md)

### 1. [Lab Title](labs/01_first_lab/Readme.md)

### 2. [Lab Title](labs/02_second_lab/Readme.md)

---

<!-- Feel free to add additional materials -->

<!-- Examples:
     Recommended Reading
     General course notes
     Other links and reference materials
-->

---

### Contributing

Feel free to submit an Issue or open a Merge Request against this repository if you notice any mis-spellings, glaring omissions, or opportunities for improvement.
